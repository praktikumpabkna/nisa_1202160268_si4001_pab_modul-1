package com.example.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edtAlas;
    private EditText edtTinggi;
    private TextView edtHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void hitung(View view){
        edtAlas = findViewById(R.id.text);
        edtTinggi =  findViewById(R.id.text2);
        edtHasil = findViewById(R.id.hasil);

        Integer alas = Integer.parseInt(edtAlas.getText().toString());
        Integer tinggi = Integer.parseInt(edtTinggi.getText().toString());
        Integer hasil = alas*tinggi;
        edtHasil.setText(String.valueOf(hasil));
    }
}
